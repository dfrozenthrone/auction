package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bij.auction.adapter.AuctionsAdapter;
import com.bij.auction.adapter.MyBidsAdapter;
import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.model.AuctionsModel;
import com.bij.auction.util.JSONParser;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MyBidFragment extends Fragment{
	
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray my_bids = null;
	MyBidsAdapter adapter;
	ArrayList<HashMap<String, String>> my_bidsList;
	DatabaseHandler db;
	
	SharedPreferences sp;
	
	String user_id;
	
	String auction_id, auction_type, category_id, owner_id, category, title, description, picture_link, start_price="0", amount_increment, bid_price, highest_bid, expected_price, owner_name="0", email="0", contact_no="0", expiry_time, status;
	ListView listview;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_my_bid, container, false);
		
		sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
		user_id = sp.getString("user_id", "");
		
		my_bidsList = new ArrayList<HashMap<String, String>>();
        adapter = new MyBidsAdapter(getActivity(), R.layout.row_my_bid, my_bidsList);
        
        listview = (ListView) rootView.findViewById(R.id.listView1);
        listview.setAdapter(adapter);
        
        new LoadAllBids().execute();
        
        listview.setOnItemClickListener(new OnItemClickListener() {
 			@SuppressLint("NewApi")
			@Override
 			public void onItemClick(AdapterView<?> parent, View view,
 					int position, long id) {
 				
// 				Toast.makeText(getActivity(), tv.getText().toString(), Toast.LENGTH_SHORT).show();
 				
 				MyBidDetailsFragment mbdf = new MyBidDetailsFragment();

				Bundle args = new Bundle();
				args.putString(AppConstants.TAG_AUCTION_ID, my_bidsList.get(position).get(AppConstants.TAG_AUCTION_ID));
				args.putString(AppConstants.TAG_TITLE, my_bidsList.get(position).get(AppConstants.TAG_TITLE));
				args.putString(AppConstants.TAG_DESCRIPTION, my_bidsList.get(position).get(AppConstants.TAG_DESCRIPTION));
				args.putString(AppConstants.TAG_PICTURE_LINK, my_bidsList.get(position).get(AppConstants.TAG_PICTURE_LINK));
				args.putString(AppConstants.TAG_AMOUNT_INCREMENT, my_bidsList.get(position).get(AppConstants.TAG_AMOUNT_INCREMENT));
				args.putString(AppConstants.TAG_BID_PRICE, my_bidsList.get(position).get(AppConstants.TAG_BID_PRICE));
				args.putString(AppConstants.TAG_HIGHEST_BID, my_bidsList.get(position).get(AppConstants.TAG_HIGHEST_BID));
				args.putString(AppConstants.TAG_EXPIRY_TIME, my_bidsList.get(position).get(AppConstants.TAG_EXPIRY_TIME));
				mbdf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_my_bid, mbdf);
				transaction.addToBackStack(null);
				transaction.commit();
 			}
 		});
		
		return rootView;
	}
	
class LoadAllBids extends AsyncTask<String, String, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("bidder_id", user_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(AppConstants.url_my_bids, "GET", params);
			
			Log.d("All auctions: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(AppConstants.TAG_SUCCESS);

				if (success == 1) {
					// events found
					// Getting Array of Events
					my_bids = json.getJSONArray(AppConstants.TAG_BIDS);

					// looping through All Events
					for (int i = 0; i < my_bids.length(); i++) {
						JSONObject c = my_bids.getJSONObject(i);

						// Storing each json item in variable
						auction_id = c.getString(AppConstants.TAG_AUCTION_ID);
						title = c.getString(AppConstants.TAG_TITLE);
						description = c.getString(AppConstants.TAG_DESCRIPTION);
						picture_link = c.getString(AppConstants.TAG_PICTURE_LINK);
						amount_increment = c.getString(AppConstants.TAG_AMOUNT_INCREMENT);
						bid_price = c.getString("user_bidamount");
						highest_bid = c.getString(AppConstants.TAG_HIGHEST_BID);
						expiry_time = c.getString(AppConstants.TAG_EXPIRY_TIME);
						
						Double my_bid = Double.parseDouble(bid_price);
						Double highest = Double.parseDouble(highest_bid);
						
						if(my_bid.doubleValue() == highest.doubleValue())
						{
							status = "Winning";
						}
						else if(my_bid.doubleValue() < highest.doubleValue())
						{
							status = "Losing";
						}
												
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(AppConstants.TAG_AUCTION_ID, auction_id);
						map.put(AppConstants.TAG_TITLE, title);
						map.put(AppConstants.TAG_DESCRIPTION, description);
						map.put(AppConstants.TAG_PICTURE_LINK, picture_link);
						map.put(AppConstants.TAG_AMOUNT_INCREMENT, amount_increment);
						map.put(AppConstants.TAG_BID_PRICE, bid_price);
						map.put(AppConstants.TAG_HIGHEST_BID, highest_bid);
						map.put(AppConstants.TAG_EXPIRY_TIME, expiry_time);
						map.put(AppConstants.TAG_STATUS, status);

						// adding HashList to ArrayList
						my_bidsList.add(map);
					}
				} else {
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
				pDialog.dismiss();
				adapter.notifyDataSetChanged();
		}

	}

}
