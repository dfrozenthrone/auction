package com.bij.auction;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class SellerDescriptionFragmentXXX extends Fragment{
	
	ImageView iv;
	EditText et1, et2, et3;
	
	SharedPreferences sp;
    Editor edt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_seller_description, container, false);
		
		sp = getActivity().getSharedPreferences("newFile", Context.MODE_PRIVATE);
		
		et1 = (EditText)rootView.findViewById(R.id.editText1);
		et2 = (EditText)rootView.findViewById(R.id.editText2);
		et3 = (EditText)rootView.findViewById(R.id.editText3);
		
		et1.setText(sp.getString("full_name", ""));
		et2.setText(sp.getString("email", ""));
		et3.setText(sp.getString("contact", ""));
		
		iv = (ImageView)rootView.findViewById(R.id.imageView1);
		iv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(et1.getText().toString().trim().equals("") ||
					et2.getText().toString().trim().equals("") ||
					et3.getText().toString().trim().equals(""))
				{
					Toast.makeText(getActivity(), "Fill all fields.", Toast.LENGTH_SHORT).show();
				}
				else
				{
					edt = sp.edit();
					edt.putString("full_name", et1.getText().toString().trim());
					edt.putString("email", et2.getText().toString().trim());
					edt.putString("contact", et3.getText().toString().trim());
					edt.commit();
					Toast.makeText(getActivity(), "Saved.", Toast.LENGTH_SHORT).show();
					getActivity().getFragmentManager().popBackStack();
				}
			}
		});
		
		return rootView;
	}

}
