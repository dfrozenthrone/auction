package com.bij.auction.model;

public class AuctionsModel {
	
	String auction_id, auction_type, category_id, owner_id, category, title, description, picture_link, start_price, amount_increment, bid_price, expected_price, owner_name, email, contact_no, expiry_time;

	public AuctionsModel()
	{
		
	}
	
	public AuctionsModel(String auction_id, String category_id, String owner_id, String category, String title, String description, String picture_link, String start_price, String amount_increment, String bid_price, String expected_price, String owner_name, String email, String contact_no, String expiry_time, String auction_type)
	{
		this.auction_id = auction_id;
		this.auction_type = auction_type;
		this.category_id = category_id;
		this.owner_id = owner_id;
		this.category = category;
		this.title = title;
		this.description = description;
		this.picture_link = picture_link;
		this.start_price = start_price;
		this.amount_increment = amount_increment;
		this.bid_price = bid_price;
		this.expected_price = expected_price;
		this.owner_name = owner_name;
		this.email = email;
		this.contact_no = contact_no;
		this.expiry_time = expiry_time;
	}

	public String getAuction_id() {
		return auction_id;
	}

	public void setAuction_id(String auction_id) {
		this.auction_id = auction_id;
	}

	public String getAuction_type() {
		return auction_type;
	}

	public void setAuction_type(String auction_type) {
		this.auction_type = auction_type;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getOwner_id() {
		return owner_id;
	}

	public void setOwner_id(String owner_id) {
		this.owner_id = owner_id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture_link() {
		return picture_link;
	}

	public void setPicture_link(String picture_link) {
		this.picture_link = picture_link;
	}

	public String getStart_price() {
		return start_price;
	}

	public void setStart_price(String start_price) {
		this.start_price = start_price;
	}

	public String getAmount_increment() {
		return amount_increment;
	}

	public void setAmount_increment(String amount_increment) {
		this.amount_increment = amount_increment;
	}

	public String getBid_price() {
		return bid_price;
	}

	public void setBid_price(String bid_price) {
		this.bid_price = bid_price;
	}

	public String getExpected_price() {
		return expected_price;
	}

	public void setExpected_price(String expected_price) {
		this.expected_price = expected_price;
	}

	public String getOwner_name() {
		return owner_name;
	}

	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact_no() {
		return contact_no;
	}

	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public String getExpiry_time() {
		return expiry_time;
	}

	public void setExpiry_time(String expiry_time) {
		this.expiry_time = expiry_time;
	}
}
