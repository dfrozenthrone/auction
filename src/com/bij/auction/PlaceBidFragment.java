package com.bij.auction;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bij.auction.util.JSONParser;

public class PlaceBidFragment extends Fragment{
	
	String bid_or_buy, auction_id, bid_price, expected_price, amount_increment;
	float price, inc;
	TextView tv, tv1;
	ImageView btn;
	EditText et;
	int success;
	
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();
	JSONArray items = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_place_bid, container, false);
		
		bid_or_buy = getArguments().getString("bid_or_buy");
		auction_id = getArguments().getString("auction_id");
		bid_price = getArguments().getString("bid_price");
		expected_price = getArguments().getString("expected_price");
		amount_increment = getArguments().getString("amount_increment");
		
		price = Float.parseFloat(bid_price);
		inc = Float.parseFloat(amount_increment);
		
		et = (EditText)rootView.findViewById(R.id.editText1);
		
		tv = (TextView)rootView.findViewById(R.id.textView2);
		tv1 = (TextView)rootView.findViewById(R.id.textView1);
		
		if(bid_or_buy.equals("buy"))
		{
			tv1.setText("Do you want to directly buy this item?");
			tv.setText(Html.fromHtml("(Rs. "+Float.parseFloat(expected_price)+")"));
			et.setVisibility(EditText.INVISIBLE);
		}
		
		else {
			tv.setText(Html.fromHtml("(Rs. "+(price+inc)+" or more (multiple of "+inc+"))"));
		}
		
		btn = (ImageView)rootView.findViewById(R.id.imageView1);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
				String flag = sp.getString("flag", "0");
				
				if(bid_or_buy.equals("bid"))
				{
					if(!et.getText().toString().equals("") && !et.getText().toString().equals("."))
					{
						if(Float.parseFloat(et.getText().toString())>=(price+inc) && Float.parseFloat(et.getText().toString())%inc==0)
						{							
							if(flag.equals("0"))
							{
								Toast.makeText(getActivity(), "Login to place your bid.", Toast.LENGTH_LONG).show();
							}
							else
							{
								AlertDialog diaBox = AskBid();
								diaBox.show();
							}
						}
						else
						{
							Toast.makeText(getActivity(), "Please bid higher or multiple of "+inc+"!", Toast.LENGTH_LONG).show();
						}
					}
					else
					{
						Toast.makeText(getActivity(), "Enter bid amount first!", Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					if(flag.equals("0"))
					{
						Toast.makeText(getActivity(), "Login to buy.", Toast.LENGTH_LONG).show();
					}
					else
					{
						AlertDialog diaBox = AskBuy();
						diaBox.show();
					}
				}
			}
		});
		
		return rootView;
	}
	
	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
	
	private AlertDialog AskBid()
	 {
	    AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity()) 
	        //set message, title, and icon
	        .setTitle("Confirm") 
	        .setMessage("Confirm bid?") 
	        .setIcon(android.R.drawable.ic_dialog_alert)

	        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	            public void onClick(DialogInterface dialog, int whichButton) {
	            	
	            	if(!haveNetworkConnection())
					{
						Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
					}
	            	else
	            	{
		            	new NewBid().execute();
		            	dialog.dismiss();
	            	}
	            	
	            }   

	        })
	        
	        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {

	                dialog.dismiss();

	            }
	        })
	        
	        .create();
	        return myQuittingDialogBox;

	 }
	
	private AlertDialog AskBuy()
	 {
	    AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity()) 
	        //set message, title, and icon
	        .setTitle("Confirm") 
	        .setMessage("Confirm buy?") 
	        .setIcon(android.R.drawable.ic_dialog_alert)

	        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	            public void onClick(DialogInterface dialog, int whichButton) {
	            	
	            	if(!haveNetworkConnection())
					{
						Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
					}
	            	else
	            	{
		            	new Buy().execute();
		            	dialog.dismiss();
	            	}
	            	
	            }   

	        })
	        
	        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {

	                dialog.dismiss();

	            }
	        })
	        
	        .create();
	        return myQuittingDialogBox;

	 }
	
	private AlertDialog ErrorMessage()
	 {
	    AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity()) 
	        //set message, title, and icon
	        .setTitle("Oops!") 
	        .setMessage("Maybe someone already placed higher bid than yours. Try higher.") 
	        .setIcon(android.R.drawable.ic_dialog_alert)

	        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	            public void onClick(DialogInterface dialog, int whichButton) {
	            	
//	            	if(!haveNetworkConnection())
//					{
//						Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
//					}
//	            	else
//	            	{
//		            	new Buy().execute();
		            	dialog.dismiss();
//	            	}
	            	
	            }   

	        })
	        
//	        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//	            public void onClick(DialogInterface dialog, int which) {
//
//	                dialog.dismiss();
//
//	            }
//	        })
	        
	        .create();
	        return myQuittingDialogBox;

	 }
	
	class NewBid extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Placing your bid..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			
			SharedPreferences sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
			String bidder_id = sp.getString("user_id", "");
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("auction_id", auction_id));
			params.add(new BasicNameValuePair("bidder_id", bidder_id));
			params.add(new BasicNameValuePair("price", et.getText().toString()));
			
			JSONObject json = jsonParser.makeHttpRequest(AppConstants.url_place_bid, "POST", params);
			
			Log.d("Create Response", json.toString());

			try {
				success = json.getInt(AppConstants.TAG_SUCCESS);

				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {		
			pDialog.dismiss();
			if (success == 1) {
				Toast.makeText(getActivity(), "Bid placed successfully", Toast.LENGTH_SHORT).show();
				getActivity().getFragmentManager().popBackStack();
				getActivity().getFragmentManager().popBackStack();
				getActivity().getFragmentManager().popBackStack();
			} else if(success == 3){
//				Toast.makeText(getActivity(), "Oops! Maybe someone already placed higher bid than yours. Try higher.", Toast.LENGTH_SHORT).show();
				AlertDialog diaBox = ErrorMessage();
				diaBox.show();
			}
			else
			{
				Toast.makeText(getActivity(), "Bid couldn't be placed", Toast.LENGTH_SHORT).show();
			}
			
		}

	}
	
	class Buy extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Confirming your purchase..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			
			SharedPreferences sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
			String bidder_id = sp.getString("user_id", "");
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("auction_id", auction_id));
			params.add(new BasicNameValuePair("bidder_id", bidder_id));
			
			JSONObject json = jsonParser.makeHttpRequest(AppConstants.url_buy, "POST", params);
			
			Log.d("Create Response", json.toString());

			try {
				success = json.getInt(AppConstants.TAG_SUCCESS);

				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {		
			pDialog.dismiss();
			if (success == 1) {
				Toast.makeText(getActivity(), "Purchased successfully!", Toast.LENGTH_SHORT).show();
				getActivity().getFragmentManager().popBackStack();
				getActivity().getFragmentManager().popBackStack();
				getActivity().getFragmentManager().popBackStack();
			} else {
				Toast.makeText(getActivity(), "Purchase wasn't successful!", Toast.LENGTH_SHORT).show();
			}
			
		}

	}

}
