package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bij.auction.adapter.BuyAdapter;
import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.model.AuctionsModel;
import com.bij.auction.util.JSONParser;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class BuyFragment extends Fragment {
	
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> categoriesList;
	JSONArray categories = null;
	ArrayList<HashMap<String, String>> auctionsList;
	JSONArray auctions = null;
	DatabaseHandler db;
	BuyAdapter adapter;
	
	String auction_id, auction_type, category_id, owner_id, category, title, description, picture_link, start_price, bid_price, expected_price, owner_name, email, contact_no, expiry_time;
	String category_image;
	
	GridView gridView;
	TextView tv;
	Button btn;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_buy, container, false);
        
        auctionsList = new ArrayList<HashMap<String, String>>();
        categoriesList = new ArrayList<HashMap<String, String>>();
        adapter = new BuyAdapter(getActivity(), R.layout.row_buy, categoriesList);
		db = new DatabaseHandler(getActivity());		

        gridView = (GridView) rootView.findViewById(R.id.gridView1);
        gridView.setAdapter(adapter);
        tv = (TextView)rootView.findViewById(R.id.textView1);
        btn = (Button)rootView.findViewById(R.id.button1);
        		
		if(haveNetworkConnection())
		{
			gridView.setVisibility(GridView.VISIBLE);
			tv.setVisibility(TextView.INVISIBLE);
			btn.setVisibility(Button.INVISIBLE);
        	new LoadCategories().execute();
		}
		else
        {
			tv.setVisibility(TextView.VISIBLE);
			btn.setVisibility(Button.VISIBLE);
			gridView.setVisibility(GridView.INVISIBLE);
//        	Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
        }
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(haveNetworkConnection())
				{
					gridView.setVisibility(GridView.VISIBLE);
					tv.setVisibility(TextView.INVISIBLE);
					btn.setVisibility(Button.INVISIBLE);
		        	new LoadCategories().execute();
				}
				else
		        {
					tv.setVisibility(TextView.VISIBLE);
					btn.setVisibility(Button.VISIBLE);
					gridView.setVisibility(GridView.INVISIBLE);
//		        	Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
		        }
			}
		});
                
        gridView.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, 
            View v, int position, long id) 
            {                
//                Toast.makeText(getActivity(), categoriesList.get(position).get("category_id"), Toast.LENGTH_SHORT).show();
                
                category_id = categoriesList.get(position).get("category_id");
                
                if(haveNetworkConnection())
        		{
//        			db.deleteAuctions();
//                	new LoadAllAuctions().execute();
                	AuctionsFragment af = new AuctionsFragment();

    				Bundle args = new Bundle();
    				args.putString("position", category_id.toString());
    				af.setArguments(args);
    				
    				FragmentTransaction transaction = getFragmentManager().beginTransaction();
    				// Replace whatever is in the fragment_container view with this fragment,
    				// and add the transaction to the back stack
    				transaction.replace(R.id.fragment_buy, af);
    				transaction.addToBackStack(null);
    				transaction.commit();
        		}
                else
                {
                	Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
                }
               
            }
        });
        
        return rootView;
    }
	
	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
	
	class LoadCategories extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(AppConstants.url_categories, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("Categories JSON: ", json.toString());

			try {
				categories = json.getJSONArray(AppConstants.TAG_CATEGORIES);
				// looping through All messages
				for (int i = 0; i < categories.length(); i++) {
					JSONObject c = categories.getJSONObject(i);

					// Storing each json item in variable
//					id = c.getString(TAG_ID);
					category_id = c.getString(AppConstants.TAG_CATEGORY_ID);
					category_image = c.getString(AppConstants.TAG_CATEGORY_IMAGE);				

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
//					map.put(TAG_ID, id);
					map.put(AppConstants.TAG_CATEGORY_ID, category_id);
					map.put(AppConstants.TAG_CATEGORY_IMAGE, category_image);

					// adding HashList to ArrayList
					categoriesList.add(map);
					
					//db.addContact(new OrdersCache(id, c_id, o_id, c_name, c_address, c_contact, lat, lon, order_status));
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			adapter.notifyDataSetChanged();

		}

	}
}
