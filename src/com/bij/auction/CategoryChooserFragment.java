package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bij.auction.BuyFragment.LoadCategories;
import com.bij.auction.adapter.BuyAdapter;
import com.bij.auction.adapter.CategoryChooserAdapter;
import com.bij.auction.util.JSONParser;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CategoryChooserFragment extends Fragment{
	
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> categoriesList;
	JSONArray categories = null;
	
	CategoryChooserAdapter adapter;
	ListView listview;
	
	String category_id, category_name, category_image;
    
    SharedPreferences sp;
    Editor edt;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_category_chooser, container, false);
		
		sp = getActivity().getSharedPreferences("newFile", Context.MODE_PRIVATE);
		
		categoriesList = new ArrayList<HashMap<String, String>>();
        adapter = new CategoryChooserAdapter(getActivity(), R.layout.row_category_chooser, categoriesList);
        
        listview = (ListView)rootView.findViewById(R.id.listView1);
        listview.setAdapter(adapter);
        
        if(haveNetworkConnection())
		{
        	new LoadCategories().execute();
		}
		else
        {
        	Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
        }
        
        listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				edt = sp.edit();
				edt.putString("category_id", categoriesList.get(position).get("category_id"));
				edt.commit();
				Toast.makeText(getActivity(), "Category Chosen.", Toast.LENGTH_SHORT).show();
				getActivity().getFragmentManager().popBackStack();
			}
		});
		
		return rootView;
	}
	
	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
	
	class LoadCategories extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(AppConstants.url_categories, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("Categories JSON: ", json.toString());

			try {
				categories = json.getJSONArray(AppConstants.TAG_CATEGORIES);
				// looping through All messages
				for (int i = 0; i < categories.length(); i++) {
					JSONObject c = categories.getJSONObject(i);

					// Storing each json item in variable
//					id = c.getString(TAG_ID);
					category_id = c.getString(AppConstants.TAG_CATEGORY_ID);
					category_name = c.getString(AppConstants.TAG_CATEGORY_NAME);
					category_image = c.getString(AppConstants.TAG_CATEGORY_IMAGE);				

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
//					map.put(TAG_ID, id);
					map.put(AppConstants.TAG_CATEGORY_ID, category_id);
					map.put(AppConstants.TAG_CATEGORY_NAME, category_name);
					map.put(AppConstants.TAG_CATEGORY_IMAGE, category_image);

					// adding HashList to ArrayList
					categoriesList.add(map);
					
					//db.addContact(new OrdersCache(id, c_id, o_id, c_name, c_address, c_contact, lat, lon, order_status));
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			adapter.notifyDataSetChanged();

		}

	}
	
}
