package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bij.auction.adapter.AuctionsAdapter;
import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.model.AuctionsModel;
import com.bij.auction.util.EndlessListView;
import com.bij.auction.util.JSONParser;

public class AuctionsFragment extends Fragment {
	// implements EndlessListView.EndlessListener{

	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray auctions = null;
	AuctionsAdapter adapter;
	ArrayList<HashMap<String, String>> auctionsList;
	DatabaseHandler db;
	ProgressBar progressBar;

	ListView listview;
	TextView tv, tv1, tv2;
	String auction_id, auction_type, category_id, owner_id, category, title,
			description, picture_link, start_price = "0", amount_increment,
			bid_price, expected_price, owner_name = "0", email = "0",
			contact_no = "0", expiry_time;

	EndlessListView lv;
	Integer j = 1;

	List<AuctionsModel> auction;
	LayoutInflater inflater;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_auctions, container,
				false);

		category_id = getArguments().getString("position");
		auctionsList = new ArrayList<HashMap<String, String>>();
		adapter = new AuctionsAdapter(getActivity(), R.layout.row_auctions,
				auctionsList);
		db = new DatabaseHandler(getActivity());

		listview = (ListView) rootView.findViewById(R.id.listView1);
		progressBar = new ProgressBar(getActivity());
		//listview.addFooterView(progressBar);
		
		
		//listview.setOnScrollListener(new EndlessScrollListener());

		listview.setAdapter(adapter);
		
		

		new LoadAllAuctions().execute();

		listview.setOnItemClickListener(new OnItemClickListener() {
			@SuppressLint("NewApi")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				tv = (TextView) view.findViewById(R.id.auction_id);
				tv1 = (TextView) view.findViewById(R.id.category_id);
				tv2 = (TextView) view.findViewById(R.id.owner_id);

				// Toast.makeText(getActivity(), tv.getText().toString(),
				// Toast.LENGTH_SHORT).show();

				ItemDetailsFragment idf = new ItemDetailsFragment();

				Bundle args = new Bundle();
				args.putString("auction_id", tv.getText().toString());
				args.putString(
						AppConstants.TAG_AUCTION_TYPE,
						auctionsList.get(position).get(
								AppConstants.TAG_AUCTION_TYPE));
				// args.putString("category_id", tv1.getText().toString());
				// args.putString("owner_id", tv2.getText().toString());
				idf.setArguments(args);

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				// Replace whatever is in the fragment_container view with this
				// fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_auctions, idf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	}

	class LoadAllAuctions extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("category_id", category_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(
					AppConstants.url_all_auctions, "GET", params);

			Log.d("All auctions: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(AppConstants.TAG_SUCCESS);

				if (success == 1) {
					// events found
					// Getting Array of Events
					auctions = json.getJSONArray(AppConstants.TAG_AUCTIONS);

					// looping through All Events
					for (int i = 0; i < auctions.length(); i++) {
						JSONObject c = auctions.getJSONObject(i);

						// Storing each json item in variable
						auction_id = c.getString(AppConstants.TAG_AUCTION_ID);
						auction_type = c
								.getString(AppConstants.TAG_AUCTION_TYPE);
						// category_id =
						// c.getString(AppConstants.TAG_CATEGORY_ID);
						owner_id = c.getString(AppConstants.TAG_OWNER_ID);
						category = c.getString(AppConstants.TAG_CATEGORY);
						title = c.getString(AppConstants.TAG_TITLE);
						description = c.getString(AppConstants.TAG_DESCRIPTION);
						picture_link = c
								.getString(AppConstants.TAG_PICTURE_LINK);
						// start_price =
						// c.getString(AppConstants.TAG_START_PRICE);
						amount_increment = c
								.getString(AppConstants.TAG_AMOUNT_INCREMENT);
						bid_price = c.getString(AppConstants.TAG_BID_PRICE);
						expected_price = c
								.getString(AppConstants.TAG_EXPECTED_PRICE);
						// owner_name =
						// c.getString(AppConstants.TAG_OWNER_NAME);
						// email = c.getString(AppConstants.TAG_EMAIL);
						// contact_no =
						// c.getString(AppConstants.TAG_CONTACT_NO);
						expiry_time = c.getString(AppConstants.TAG_EXPIRY_TIME);

						if (bid_price.equals(null) || bid_price.equals("null")) {
							bid_price = "0";
						}

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(AppConstants.TAG_AUCTION_ID, auction_id);
						map.put(AppConstants.TAG_AUCTION_TYPE, auction_type);
						map.put(AppConstants.TAG_CATEGORY_ID, category_id);
						map.put(AppConstants.TAG_OWNER_ID, owner_id);
						map.put(AppConstants.TAG_CATEGORY, category);
						map.put(AppConstants.TAG_TITLE, title);
						map.put(AppConstants.TAG_DESCRIPTION, description);
						map.put(AppConstants.TAG_PICTURE_LINK, picture_link);
						// map.put(AppConstants.TAG_START_PRICE, start_price);
						map.put(AppConstants.TAG_AMOUNT_INCREMENT,
								amount_increment);
						map.put(AppConstants.TAG_BID_PRICE, bid_price);
						map.put(AppConstants.TAG_EXPECTED_PRICE, expected_price);
						// map.put(AppConstants.TAG_OWNER_NAME, owner_name);
						// map.put(AppConstants.TAG_EMAIL, email);
						// map.put(AppConstants.TAG_CONTACT_NO, contact_no);
						map.put(AppConstants.TAG_EXPIRY_TIME, expiry_time);

						// adding HashList to ArrayList
						auctionsList.add(map);

						db.addAuction(new AuctionsModel(auction_id,
								category_id, owner_id, category, title,
								description, picture_link, start_price,
								amount_increment, bid_price, expected_price,
								owner_name, email, contact_no, expiry_time,
								auction_type));
					}
				} else {
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			int currentPosition = listview.getFirstVisiblePosition();

			adapter.notifyDataSetChanged();
			listview.setSelectionFromTop(currentPosition + 1, 0);
			Integer count = listview.getCount()-1;
			if(count==0)
			{
				listview.removeFooterView(progressBar);
			}
			
			
				
			
			Log.e("No of listview counts", count.toString());
			
		}

	}

	class EndlessScrollListener implements OnScrollListener {

		/*
		 * private int visibleThreshold = 5;
		 * 
		 * private int previousTotal = 0; private boolean loading = true;
		 */
		private int threshold = 0;
		private int currentPage = 0;

		@Override
		public void onScroll(AbsListView arg0, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub
			/*
			 * if (loading) { if (totalItemCount > previousTotal) { loading =
			 * false; previousTotal = totalItemCount; currentPage++; } } if
			 * (!loading & (totalItemCount - visibleItemCount) <=
			 * (firstVisibleItem + visibleThreshold)) { // I load the next page
			 * of gigs using a background task, // but you can call any function
			 * here.
			 * 
			 * new LoadAllProducts().execute();
			 * 
			 * adapter.notifyAll();
			 * 
			 * loading = true; }
			 */

		}

		@Override
		public void onScrollStateChanged(AbsListView listView, int scrollState) {
			// TODO Auto-generated method stub

			if (scrollState == SCROLL_STATE_IDLE) {
				if (listView.getLastVisiblePosition() >= listView.getCount()
						- 1 - threshold) {
					currentPage++;
					// load more list items:
					
					new LoadAllAuctions().execute();
					

				}
			}

		}

	}

	/*
	 * private class FakeNetLoader extends AsyncTask<String, Void,
	 * ArrayList<HashMap<String, String>>> {
	 * 
	 * @Override protected ArrayList<HashMap<String, String>>
	 * doInBackground(String... params) { try { Thread.sleep(4000); } catch
	 * (InterruptedException e) { e.printStackTrace(); } // result.clear();
	 * return createItems(); }
	 * 
	 * @Override protected void onPostExecute(ArrayList<HashMap<String, String>>
	 * result) { super.onPostExecute(result); lv.addNewData(result); }
	 * 
	 * 
	 * 
	 * }
	 */

	/*
	 * private ArrayList<HashMap<String, String>> createItems() {
	 * 
	 * auctionsList = new ArrayList<HashMap<String, String>>();
	 * 
	 * 
	 * 
	 * for (AuctionsModel i : auction) { auction_id = i.getAuction_id(); //
	 * category_id = i.getCategory_id(); owner_id = i.getOwner_id(); category =
	 * i.getCategory(); title = i.getTitle(); description = i.getDescription();
	 * picture_link = i.getPicture_link(); start_price = i.getStart_price();
	 * bid_price = i.getBid_price(); expected_price = i.getExpected_price();
	 * owner_name = i.getOwner_name(); email = i.getEmail(); contact_no =
	 * i.getContact_no(); expiry_time = i.getExpiry_time();
	 * 
	 * // Log.d("AUCTION_ID", auction_id);
	 * 
	 * // creating new HashMap HashMap<String, String> map = new HashMap<String,
	 * String>();
	 * 
	 * // adding each child node to HashMap key => value
	 * map.put(AppConstants.TAG_AUCTION_ID, auction_id);
	 * map.put(AppConstants.TAG_CATEGORY_ID, category_id);
	 * map.put(AppConstants.TAG_OWNER_ID, owner_id);
	 * map.put(AppConstants.TAG_CATEGORY, category);
	 * map.put(AppConstants.TAG_TITLE, title);
	 * map.put(AppConstants.TAG_DESCRIPTION, description);
	 * map.put(AppConstants.TAG_PICTURE_LINK, picture_link);
	 * map.put(AppConstants.TAG_START_PRICE, start_price);
	 * map.put(AppConstants.TAG_BID_PRICE, bid_price);
	 * map.put(AppConstants.TAG_EXPECTED_PRICE, expected_price);
	 * map.put(AppConstants.TAG_OWNER_NAME, owner_name);
	 * map.put(AppConstants.TAG_EMAIL, email);
	 * map.put(AppConstants.TAG_CONTACT_NO, contact_no);
	 * map.put(AppConstants.TAG_EXPIRY_TIME, expiry_time);
	 * 
	 * // adding HashList to ArrayList auctionsList.add(map); }
	 * 
	 * return auctionsList; }
	 */

	// @Override
	// public void loadData() {
	// // System.out.println("Load data");
	// // mult += 10;
	// // We load more data here
	// FakeNetLoader fl = new FakeNetLoader();
	// fl.execute(new String[]{});
	//
	// }

}
