package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.model.AuctionsModel;
import com.bij.auction.util.JSONParser;

public class SplashScreen extends Activity{
	
	private static int SPLASH_TIME_OUT = 5000;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		final ActionBar actionBar = getActionBar(); 
		BitmapDrawable background = new BitmapDrawable (BitmapFactory.decodeResource(getResources(), R.drawable.actionbarbg)); 
		background.setTileModeX(android.graphics.Shader.TileMode.REPEAT); 
		actionBar.setBackgroundDrawable(background);		
		
		new Handler().postDelayed(new Runnable() {
			
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                
                finish();
            }
        }, SPLASH_TIME_OUT);
	}
	
	

}
