package com.bij.auction;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VerificationFragment extends Fragment{
	
	String code;
	TextView tv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_verification, container, false);
		
		code = getArguments().getString("code");
		
		tv = (TextView)rootView.findViewById(R.id.textView2);
		tv.setText(code);
		
		return rootView;
	}

}
