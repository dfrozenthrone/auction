package com.bij.auction.database;

import java.util.ArrayList;
import java.util.List;

import com.bij.auction.model.AuctionsModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper{
	
	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 4;

	// Database Name
	private static final String DATABASE_NAME = "db_auction";

	// Events table name
	private static final String AUCTIONS = "auctions";
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_AUCTIONS_TABLE = "CREATE TABLE auctions (auction_id TEXT, category_id TEXT, owner_id TEXT, category TEXT, title TEXT, description TEXT, picture_link TEXT, start_price TEXT, amount_increment TEXT, bid_price TEXT, expected_price TEXT, owner_name TEXT, email TEXT, contact_no TEXT, expiry_time TEXT, auction_type TEXT) ";
		db.execSQL(CREATE_AUCTIONS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + AUCTIONS);
		
		// Create tables again
		onCreate(db);
	}
	
	public void addAuction(AuctionsModel auction) {
		SQLiteDatabase db = this.getWritableDatabase();

		String sql = "INSERT OR REPLACE INTO auctions (auction_id, category_id, owner_id, category, title, description, picture_link, start_price, amount_increment, bid_price, expected_price, owner_name, email, contact_no, expiry_time, auction_type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQLiteStatement stmt = db.compileStatement(sql);
		
		ContentValues values = new ContentValues();
		
		  stmt.bindString(1, auction.getAuction_id());
		  stmt.bindString(2, auction.getCategory_id());
		  stmt.bindString(3, auction.getOwner_id());
		  stmt.bindString(4, auction.getCategory());
		  stmt.bindString(5, auction.getTitle());
		  stmt.bindString(6, auction.getDescription());
		  stmt.bindString(7, auction.getPicture_link());
		  stmt.bindString(8, auction.getStart_price());
		  stmt.bindString(9, auction.getAmount_increment());
		  stmt.bindString(10, auction.getBid_price());
		  stmt.bindString(11, auction.getExpected_price());
		  stmt.bindString(12, auction.getOwner_name());
		  stmt.bindString(13, auction.getEmail());
		  stmt.bindString(14, auction.getContact_no());
		  stmt.bindString(15, auction.getExpiry_time());
		  stmt.bindString(16, auction.getAuction_type());
		  
		stmt.execute();
		
		db.close(); // Closing database connection
	}
	
	public List<AuctionsModel> getAllAuctions() {
		List<AuctionsModel> auctionsList = new ArrayList<AuctionsModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + AUCTIONS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				AuctionsModel auctions = new AuctionsModel();
				auctions.setAuction_id(cursor.getString(0));
				auctions.setCategory_id(cursor.getString(1));
				auctions.setOwner_id(cursor.getString(2));
				auctions.setCategory(cursor.getString(3));
				auctions.setTitle(cursor.getString(4));
				auctions.setDescription(cursor.getString(5));
				auctions.setPicture_link(cursor.getString(6));
				auctions.setStart_price(cursor.getString(7));
				auctions.setAmount_increment(cursor.getString(8));
				auctions.setBid_price(cursor.getString(9));
				auctions.setExpected_price(cursor.getString(10));
				auctions.setOwner_name(cursor.getString(11));
				auctions.setEmail(cursor.getString(12));
				auctions.setContact_no(cursor.getString(13));
				auctions.setExpiry_time(cursor.getString(14));
				auctions.setAuction_type(cursor.getString(15));
				// Adding contact to list
				auctionsList.add(auctions);
			} while (cursor.moveToNext());
		}
		cursor.close();
		// return contact list
		return auctionsList;
	}
	
	public List<AuctionsModel> getAuction(String auction_id) throws SQLException {
		List<AuctionsModel> auctionList = new ArrayList<AuctionsModel>();
		String sql = "SELECT * FROM " + AUCTIONS + " where auction_id = " + auction_id;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			if(cursor.moveToFirst()) {
				do {
					AuctionsModel auctionDetail = new AuctionsModel();
					auctionDetail.setAuction_id(cursor.getString(0));
					auctionDetail.setCategory_id(cursor.getString(1));
					auctionDetail.setOwner_id(cursor.getString(2));
					auctionDetail.setCategory(cursor.getString(3));
					auctionDetail.setTitle(cursor.getString(4));
					auctionDetail.setDescription(cursor.getString(5));
					auctionDetail.setPicture_link(cursor.getString(6));
					auctionDetail.setStart_price(cursor.getString(7));
					auctionDetail.setAmount_increment(cursor.getString(8));
					auctionDetail.setBid_price(cursor.getString(9));
					auctionDetail.setExpected_price(cursor.getString(10));
					auctionDetail.setOwner_name(cursor.getString(11));
					auctionDetail.setEmail(cursor.getString(12));
					auctionDetail.setContact_no(cursor.getString(13));
					auctionDetail.setExpiry_time(cursor.getString(14));
					auctionDetail.setAuction_type(cursor.getString(15));

					auctionList.add(auctionDetail);
				} while(cursor.moveToNext());
			} 
		}
		cursor.close();
		return auctionList;
	}
	
	 void deleteAuctions() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(AUCTIONS, null, null);
		db.close();
	}

	// Getting contacts Count
	public int getAuctionsCount() {
		String countQuery = "SELECT  * FROM " + AUCTIONS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}
