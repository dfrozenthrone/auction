package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bij.auction.MyBidFragment.LoadAllBids;
import com.bij.auction.adapter.AuctionsAdapter;
import com.bij.auction.adapter.MyAuctionsAdapter;
import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.util.JSONParser;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class MyAuctionFragment extends Fragment{

	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray my_auctions = null;
	MyAuctionsAdapter adapter;
	ArrayList<HashMap<String, String>> my_auctionsList;
	DatabaseHandler db;
	
	SharedPreferences sp;
	
	String user_id;
	
	String auction_id, auction_type, category_id, owner_id, category, title, description, picture_link, start_price="0", amount_increment, bid_price, highest_bid, expected_price, owner_name="0", email="0", contact_no="0", expiry_time, status;
	ListView listview;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_my_auction, container, false);
		
		sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
		user_id = sp.getString("user_id", "");
		
		my_auctionsList = new ArrayList<HashMap<String, String>>();
        adapter = new MyAuctionsAdapter(getActivity(), R.layout.row_my_auction, my_auctionsList);
        
        listview = (ListView) rootView.findViewById(R.id.listView1);
        listview.setAdapter(adapter);
        
        new LoadMyAuctions().execute();
        
        listview.setOnItemClickListener(new OnItemClickListener() {
 			@SuppressLint("NewApi")
			@Override
 			public void onItemClick(AdapterView<?> parent, View view,
 					int position, long id) {
 				
// 				Toast.makeText(getActivity(), tv.getText().toString(), Toast.LENGTH_SHORT).show();
 				
 				MyAuctionDetailsFragment madf = new MyAuctionDetailsFragment();

				Bundle args = new Bundle();
				args.putString(AppConstants.TAG_AUCTION_TYPE, my_auctionsList.get(position).get(AppConstants.TAG_AUCTION_TYPE));
				args.putString(AppConstants.TAG_TITLE, my_auctionsList.get(position).get(AppConstants.TAG_TITLE));
				args.putString(AppConstants.TAG_CATEGORY_NAME, my_auctionsList.get(position).get(AppConstants.TAG_CATEGORY_NAME));
				args.putString(AppConstants.TAG_DESCRIPTION, my_auctionsList.get(position).get(AppConstants.TAG_DESCRIPTION));
				args.putString(AppConstants.TAG_PICTURE_LINK, my_auctionsList.get(position).get(AppConstants.TAG_PICTURE_LINK));
//				args.putString(AppConstants.TAG_AMOUNT_INCREMENT, my_auctionsList.get(position).get(AppConstants.TAG_AMOUNT_INCREMENT));
				args.putString(AppConstants.TAG_BID_PRICE, my_auctionsList.get(position).get(AppConstants.TAG_BID_PRICE));
				args.putString(AppConstants.TAG_EXPECTED_PRICE, my_auctionsList.get(position).get(AppConstants.TAG_EXPECTED_PRICE));
				args.putString(AppConstants.TAG_EXPIRY_TIME, my_auctionsList.get(position).get(AppConstants.TAG_EXPIRY_TIME));
				args.putString(AppConstants.TAG_STATUS, my_auctionsList.get(position).get(AppConstants.TAG_STATUS));
				madf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_my_auction, madf);
				transaction.addToBackStack(null);
				transaction.commit();
 			}
 		});
		
		return rootView;
	}
	
class LoadMyAuctions extends AsyncTask<String, String, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("bidder_id", user_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(AppConstants.url_all_auctions, "GET", params);
			
			Log.d("All auctions: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(AppConstants.TAG_SUCCESS);

				if (success == 1) {
					// events found
					// Getting Array of Events
					my_auctions = json.getJSONArray(AppConstants.TAG_AUCTIONS);

					// looping through All Events
					for (int i = 0; i < my_auctions.length(); i++) {
						JSONObject c = my_auctions.getJSONObject(i);

						// Storing each json item in variable
//						auction_id = c.getString(AppConstants.TAG_AUCTION_ID);
						auction_type = c.getString(AppConstants.TAG_AUCTION_TYPE);
//						category_id = c.getString(AppConstants.TAG_CATEGORY_ID);
//						owner_id = c.getString(AppConstants.TAG_OWNER_ID);
						category = c.getString(AppConstants.TAG_CATEGORY_NAME);
						title = c.getString(AppConstants.TAG_TITLE);
						description = c.getString(AppConstants.TAG_DESCRIPTION);
						picture_link = c.getString(AppConstants.TAG_PICTURE_LINK);
//						start_price = c.getString(AppConstants.TAG_START_PRICE);
//						amount_increment = c.getString(AppConstants.TAG_AMOUNT_INCREMENT);
						bid_price = c.getString(AppConstants.TAG_BID_PRICE);
						expected_price = c.getString(AppConstants.TAG_EXPECTED_PRICE);
//						owner_name = c.getString(AppConstants.TAG_OWNER_NAME);
//						email = c.getString(AppConstants.TAG_EMAIL);
//						contact_no = c.getString(AppConstants.TAG_CONTACT_NO);
						expiry_time = c.getString(AppConstants.TAG_EXPIRY_TIME);
//						highest_bid = c.getString(AppConstants.TAG_HIGHEST_BID);
						status = c.getString(AppConstants.TAG_STATUS);
						
						if(bid_price.equals(null) || bid_price.equals("null"))
						{
							bid_price = "0";
						}
						
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
//						map.put(AppConstants.TAG_AUCTION_ID, auction_id);
						map.put(AppConstants.TAG_AUCTION_TYPE, auction_type);
//						map.put(AppConstants.TAG_CATEGORY_ID, category_id);
//						map.put(AppConstants.TAG_OWNER_ID, owner_id);
						map.put(AppConstants.TAG_CATEGORY_NAME, category);
						map.put(AppConstants.TAG_TITLE, title);
						map.put(AppConstants.TAG_DESCRIPTION, description);
						map.put(AppConstants.TAG_PICTURE_LINK, picture_link);
//						map.put(AppConstants.TAG_START_PRICE, start_price);
//						map.put(AppConstants.TAG_AMOUNT_INCREMENT, amount_increment);
						map.put(AppConstants.TAG_BID_PRICE, bid_price);
						map.put(AppConstants.TAG_EXPECTED_PRICE, expected_price);
//						map.put(AppConstants.TAG_OWNER_NAME, owner_name);
//						map.put(AppConstants.TAG_EMAIL, email);
//						map.put(AppConstants.TAG_CONTACT_NO, contact_no);
						map.put(AppConstants.TAG_EXPIRY_TIME, expiry_time);
						map.put(AppConstants.TAG_STATUS, status);

						// adding HashList to ArrayList
						my_auctionsList.add(map);
					}
				} else {
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
				pDialog.dismiss();
				adapter.notifyDataSetChanged();
		}

	}

}
