package com.bij.auction;

import com.bij.auction.util.ImageLoader;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class MyBidDetailsFragment extends Fragment{
	
	String auction_id, title, description, picture_link, amount_increment, bid_price, expiry_time, highest_bid;
	ImageView image, btn;
	TextView tTitle, tPrice, tExpiry, tDescription, tYourBid;
	ImageLoader imageLoader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_item_details, container, false);
		
		auction_id = getArguments().getString(AppConstants.TAG_AUCTION_ID);
		title = getArguments().getString(AppConstants.TAG_TITLE);
		description = getArguments().getString(AppConstants.TAG_DESCRIPTION);
		picture_link = getArguments().getString(AppConstants.TAG_PICTURE_LINK);
		amount_increment = getArguments().getString(AppConstants.TAG_AMOUNT_INCREMENT);
		bid_price = getArguments().getString(AppConstants.TAG_BID_PRICE);
		expiry_time = getArguments().getString(AppConstants.TAG_EXPIRY_TIME);
		highest_bid = getArguments().getString(AppConstants.TAG_HIGHEST_BID);
		
		image = (ImageView)rootView.findViewById(R.id.imageView1);
		btn = (ImageView)rootView.findViewById(R.id.imageView2);
		tTitle = (TextView)rootView.findViewById(R.id.textView1);
		tPrice = (TextView)rootView.findViewById(R.id.textView5);
		tYourBid = (TextView)rootView.findViewById(R.id.textView6);
		tExpiry = (TextView)rootView.findViewById(R.id.textView3);
		tDescription = (TextView)rootView.findViewById(R.id.textView4);
		
		btn.setVisibility(ImageView.VISIBLE);
		tYourBid.setVisibility(TextView.VISIBLE);
		
		tTitle.setText(title);
		tPrice.setText(Html.fromHtml("Rs. " +highest_bid));
		tExpiry.setText(Html.fromHtml("Expires: " +expiry_time));
		tDescription.setText(Html.fromHtml("<b>Description: </b><br/>" +description));
		tYourBid.setText(Html.fromHtml("Your bid Rs. " +bid_price));
		
		imageLoader=new ImageLoader(getActivity());
		imageLoader.DisplayImage(picture_link , image);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PlaceBidFragment pb = new PlaceBidFragment();

				Bundle args = new Bundle();
				args.putString("bid_or_buy", "bid");
				args.putString("auction_id", auction_id);
				args.putString("bid_price", highest_bid);
				args.putString("amount_increment", amount_increment);
				args.putString("expected_price", "");
				pb.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_item_details, pb);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});	
		
		return rootView;
	}

}
