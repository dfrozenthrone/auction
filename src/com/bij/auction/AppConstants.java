package com.bij.auction;

public class AppConstants {
	
	public static final String url_categories = "http://pagodalabs.com/auction/auctionsite/category/api";
	public static final String url_all_auctions = "http://pagodalabs.com/auction/auctionsite/auction/api";
	public static final String url_auction_details = "http://pagodalabs.com/auction/auctionsite/auction/api/detail";
	public static final String url_check_login = "http://pagodalabs.com/auction/auctionsite/auth/api/login";
	public static final String url_place_bid = "http://pagodalabs.com/auction/auctionsite/bid/api";
	public static final String url_buy = "http://pagodalabs.com/auction/auctionsite/buy/api";
	public static final String url_upload = "http://pagodalabs.com/auction/auctionsite/auction/api";
	public static final String url_check_username = "http://pagodalabs.com/bijesh/auction/check_username.php";
	public static final String url_create_account = "http://pagodalabs.com/auction/auctionsite/auth/api/register";
	public static final String url_my_bids = "http://pagodalabs.com/auction/auctionsite/bid/api";
	
	public static final String TAG_SUCCESS = "success";
	
	public static final String TAG_AUCTIONS = "auctions";
	public static final String TAG_BIDS = "bids";
	public static final String TAG_AUCTION_ID = "auction_id";
	public static final String TAG_AUCTION_TYPE = "auction_type";
	public static final String TAG_CATEGORY_ID = "category_id";
	public static final String TAG_OWNER_ID = "owner";
	public static final String TAG_CATEGORY = "category";
	public static final String TAG_CATEGORY_NAME = "category_name";
	public static final String TAG_TITLE = "auction_title";
	public static final String TAG_DESCRIPTION = "auction_description";
	public static final String TAG_PICTURE_LINK = "auction_image";
	public static final String TAG_START_PRICE = "start_price";
	public static final String TAG_AMOUNT_INCREMENT = "amount_increment";
	public static final String TAG_BID_PRICE = "bid_amount";
	public static final String TAG_EXPECTED_PRICE = "buy_price";
	public static final String TAG_OWNER_NAME = "owner_name";
	public static final String TAG_EMAIL = "email";
	public static final String TAG_CONTACT_NO = "contact_no";
	public static final String TAG_EXPIRY_TIME = "end_date";
	
	public static final String TAG_CATEGORIES = "categories";
	public static final String TAG_CATEGORY_IMAGE = "category_image";
	
	public static final String TAG_USERS = "users";
	public static final String TAG_USER_ID = "user_id";
	public static final String TAG_NAME = "name";
	public static final String TAG_FIRST_NAME = "first_name";
	public static final String TAG_LAST_NAME = "last_name";
	public static final String TAG_USERNAME = "username";
	public static final String TAG_CONTACT = "contact";
	
	public static final String TAG_CODE = "code";
	
	public static final String TAG_HIGHEST_BID = "high_amount";
	public static final String TAG_STATUS = "status";

}
