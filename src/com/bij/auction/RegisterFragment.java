package com.bij.auction;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bij.auction.util.JSONParser;

public class RegisterFragment extends Fragment {
	
	EditText et1, et7, et2, et3, et4, et5, et6;
	ImageView register;
	
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        
        et1 = (EditText)rootView.findViewById(R.id.editText1);
        et7 = (EditText)rootView.findViewById(R.id.editText7);
		et2 = (EditText)rootView.findViewById(R.id.editText2);
		et3 = (EditText)rootView.findViewById(R.id.editText3);
		et4 = (EditText)rootView.findViewById(R.id.editText4);
//		et5 = (EditText)rootView.findViewById(R.id.editText5);
		et6 = (EditText)rootView.findViewById(R.id.editText6);
        
        register = (ImageView)rootView.findViewById(R.id.imageView1);
        register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(et1.getText().toString().trim().equals("") || et7.getText().toString().trim().equals("") || et2.getText().toString().trim().equals("") || et3.getText().toString().trim().equals("") || et4.getText().toString().trim().equals("") || et6.getText().toString().trim().equals(""))
				{
					Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_SHORT).show();
				}
				else if(et2.getText().toString().contains(" "))
				{
					Toast.makeText(getActivity(), "No spaces allowed in username", Toast.LENGTH_SHORT).show();
				}
				else if(!et3.getText().toString().equals(et4.getText().toString()))
				{
					Toast.makeText(getActivity(), "Password do not match", Toast.LENGTH_SHORT).show();
				}
				else
				{
//					Toast.makeText(getActivity(), "kbjgjgjh", Toast.LENGTH_SHORT).show();
					new CheckUsername().execute();
				}
			}
		});
         
        return rootView;
    }
	
	class CheckUsername extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Checking..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					
					int success;
					try {

						String first_name = et1.getText().toString();
						String last_name = et7.getText().toString();
						String username = et2.getText().toString();
						String password = et3.getText().toString();
//						String contact = et5.getText().toString();
						String email = et6.getText().toString();
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("first_name", first_name));
						params.add(new BasicNameValuePair("last_name", last_name));
						params.add(new BasicNameValuePair("username", username));
						params.add(new BasicNameValuePair("password", password));
//						params.add(new BasicNameValuePair("contact", contact));
						params.add(new BasicNameValuePair("email", email));
						
						JSONObject json = jsonParser.makeHttpRequest(AppConstants.url_create_account, "POST", params);

						Log.d("Single Event Details", json.toString());
						
						success = json.getInt(AppConstants.TAG_SUCCESS);
						if (success == 1) {							
							Toast.makeText(getActivity(), "Account created successfully.", Toast.LENGTH_LONG).show();
							getActivity().getFragmentManager().popBackStack();
						}
						else if(success == 2){
							Toast.makeText(getActivity(), "Username/Email already exists.", Toast.LENGTH_LONG).show();
						}
						else{
							Toast.makeText(getActivity(), "An error occured. Please try again.", Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		protected void onPostExecute(String file_url) {
			
			pDialog.dismiss();
		}

	}
}