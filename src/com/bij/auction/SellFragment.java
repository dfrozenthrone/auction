package com.bij.auction;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.bij.auction.PlaceBidFragment.NewBid;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class SellFragment extends Fragment {
	
	ImageView btn3, btn4, btn5, submit, photo, browse;
	final static int cameraData = 0;
	private String imagepath=null;
	private int serverResponseCode = 0;
	private ProgressDialog dialog = null;
	SharedPreferences sp, sp1;
	String owner_id, category_id, title, description, price, expected_price, auction_type, full_name, email, contact, amount_increment;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_sell, container, false);
        
        sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        sp1 = getActivity().getSharedPreferences("newFile", Context.MODE_PRIVATE);
        
        btn3 = (ImageView)rootView.findViewById(R.id.imageView3);
        btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CategoryChooserFragment ccf = new CategoryChooserFragment();

				Bundle args = new Bundle();
				ccf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_sell, ccf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
        
        btn4 = (ImageView)rootView.findViewById(R.id.imageView4);
        btn4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ItemDescriptionFragment idf = new ItemDescriptionFragment();

				Bundle args = new Bundle();
				idf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_sell, idf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
        
//        btn5 = (ImageView)rootView.findViewById(R.id.imageView5);
//        btn5.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				SellerDescriptionFragment sdf = new SellerDescriptionFragment();
//
//				Bundle args = new Bundle();
//				sdf.setArguments(args);
//				
//				FragmentTransaction transaction = getFragmentManager().beginTransaction();
//				// Replace whatever is in the fragment_container view with this fragment,
//				// and add the transaction to the back stack
//				transaction.replace(R.id.fragment_sell, sdf);
//				transaction.addToBackStack(null);
//				transaction.commit();
//			}
//		});
        
        submit = (ImageView)rootView.findViewById(R.id.imageView6);
        submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String flag = sp.getString("flag", "0");
				owner_id = sp.getString("user_id", "");
				
				category_id = sp1.getString("category_id", "");
				title = sp1.getString("title", "");
				description = sp1.getString("description", "");
				expected_price = sp1.getString("expected_price", "");
				auction_type = sp1.getString("auction_type", "");
				amount_increment = sp1.getString("amount_increment", "");
//				full_name = sp1.getString("full_name", "");
//				email = sp1.getString("email", "");
//				contact = sp1.getString("contact", "");
				
				if(!haveNetworkConnection())
				{
					Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
				}
				else if(imagepath==null)
				{
					Toast.makeText(getActivity(), "Please choose an image!", Toast.LENGTH_SHORT).show();
				}
				else if(flag.equals("0"))
				{
					Toast.makeText(getActivity(), "Login to upload your auction.", Toast.LENGTH_LONG).show();
				}
				else if(category_id.equals("") || title.equals("") || description.equals("") || auction_type.equals("") || expected_price.equals(""))
				{
					Toast.makeText(getActivity(), "Some fields are empty. Please go through all fields.", Toast.LENGTH_LONG).show();
				}				
				else
				{
					AlertDialog diaBox = AskOption();
					diaBox.show();
//					Toast.makeText(getActivity(), sp.getString("category_id", "0")+sp.getString("title", "0")+sp.getString("email", "0"), Toast.LENGTH_SHORT).show();
				}
			}
		});
        
        photo = (ImageView)rootView.findViewById(R.id.imageView1);
        photo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(i, cameraData);
			}
		});
        
        browse = (ImageView)rootView.findViewById(R.id.imageView2);
        browse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
	            intent.setType("image/*");
	            intent.setAction(Intent.ACTION_GET_CONTENT);
	            startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);
			}
		});
         
        return rootView;
    }
	
	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
	
	private AlertDialog AskOption()
	 {
	    AlertDialog myQuittingDialogBox =new AlertDialog.Builder(getActivity()) 
	        //set message, title, and icon
	        .setTitle("Confirm") 
	        .setMessage("Confirm place item in auction?") 
	        .setIcon(android.R.drawable.ic_dialog_alert)

	        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	            public void onClick(DialogInterface dialog, int whichButton) {

					upload();
	            	dialog.dismiss();
	            	
	            }   

	        })
	        
	        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {

	                dialog.dismiss();

	            }
	        })
	        
	        .create();
	        return myQuittingDialogBox;

	 }
	
	public void upload()
	{
		dialog = ProgressDialog.show(getActivity(), "", "Uploading file...", true);
		 
		 new Thread(new Runnable() {
            public void run() {
                                
                 uploadFile(imagepath);
                                         
            }
          }).start();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 1 && resultCode == getActivity().RESULT_OK)
		{	          
            Uri selectedImageUri = data.getData();
            imagepath = getPath(selectedImageUri);
            Bitmap bitmap=BitmapFactory.decodeFile(imagepath);
            
            Toast.makeText(getActivity(), "Photo chosen.", Toast.LENGTH_SHORT).show();            
		}
		
		else if(resultCode == getActivity().RESULT_OK)
		{			
			Toast.makeText(getActivity(), "Photo taken. Browse to upload.", Toast.LENGTH_SHORT).show();			
		}
	}
	
	public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	public int uploadFile(String sourceFileUri) {
        
	  	  
	  	  String fileName = sourceFileUri;

	        HttpURLConnection conn = null;
	        DataOutputStream dos = null;  
	        String lineEnd = "\r\n";
	        String twoHyphens = "--";
	        String boundary = "*****";
	        int bytesRead, bytesAvailable, bufferSize;
	        byte[] buffer;
	        int maxBufferSize = 1 * 1024 * 1024; 
	        File sourceFile = new File(sourceFileUri); 
	        
	        if (!sourceFile.isFile()) {
	      	  
		           dialog.dismiss(); 
		           
		           Log.e("uploadFile", "Source File not exist :"+imagepath);
		           
		           getActivity().runOnUiThread(new Runnable() {
		               public void run() {
		            	   
		               }
		           });      
//		          {{{{{{{{{{{{{{{{{{{{{{{{{{.getName();		           
		           return 0;
	         
	        }
	        else
	        {
		           try { 
		        	   
		            	 // open a URL connection to the Servlet
		               FileInputStream fileInputStream = new FileInputStream(sourceFile);
		               URL url = new URL(AppConstants.url_upload);
		               
		               // Open a HTTP  connection to  the URL
		               conn = (HttpURLConnection) url.openConnection(); 
		               conn.setDoInput(true); // Allow Inputs
		               conn.setDoOutput(true); // Allow Outputs
		               conn.setUseCaches(false); // Don't use a Cached Copy
		               conn.setRequestMethod("POST");
		               conn.setRequestProperty("Connection", "Keep-Alive");
		               conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		               conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
		               conn.setRequestProperty("uploaded_file", fileName); 
		               
		               dos = new DataOutputStream(conn.getOutputStream());
		     
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"category_id\"" +lineEnd+lineEnd+ category_id + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"owner_id\"" +lineEnd+lineEnd+ owner_id + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"title\"" +lineEnd+lineEnd+ title + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"description\"" +lineEnd+lineEnd+ description + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"price\"" +lineEnd+lineEnd+ expected_price + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"auction_type\"" +lineEnd+lineEnd+ auction_type + lineEnd);	
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"amount_increment\"" +lineEnd+lineEnd+ amount_increment + lineEnd);
		               dos.writeBytes(twoHyphens + boundary + lineEnd); 
		               dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
		               
		               dos.writeBytes(lineEnd);
		     
		               // create a buffer of  maximum size
		               bytesAvailable = fileInputStream.available(); 
		     
		               bufferSize = Math.min(bytesAvailable, maxBufferSize);
		               buffer = new byte[bufferSize];
		     
		               // read file and write it into form...
		               bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
		                 
		               while (bytesRead > 0) {
		            	   
		                 dos.write(buffer, 0, bufferSize);
		                 bytesAvailable = fileInputStream.available();
		                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
		                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
		                 
		                }
		     
		               // send multipart form data necesssary after file data...
		               dos.writeBytes(lineEnd);
		               dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		     
		               // Responses from the server (code and message)
		               serverResponseCode = conn.getResponseCode();
		               String serverResponseMessage = conn.getResponseMessage();
		                
		               Log.i("uploadFile", "HTTP Response is : " 
		            		   + serverResponseMessage + ": " + serverResponseCode);
		               
		               if(serverResponseCode == 200){
		            	   
		                   getActivity().runOnUiThread(new Runnable() {
		                        public void run() {
		                        		                        	
		                            Toast.makeText(getActivity(), "File Upload Complete.", Toast.LENGTH_SHORT).show();
		                            
		                            imagepath = null;
		                            Editor editor = sp1.edit();
		            				editor.putString("category_id", "");
		            				editor.putString("title", "");
		            				editor.putString("description", "");
		            				editor.putString("auction_type", "");
		            				editor.putString("expected_price", "");
//		            				editor.putString("full_name", "");
//		            				editor.putString("email", "");
//		            				editor.putString("contact", "");
		            				editor.commit();
		                        }
		                    });                
		               }    
		               
		               //close the streams //
		               fileInputStream.close();
		               dos.flush();
		               dos.close();
		                
		          } catch (MalformedURLException ex) {
		        	  
		              dialog.dismiss();  
		              ex.printStackTrace();
		              
		              getActivity().runOnUiThread(new Runnable() {
		                  public void run() {
		                	  
		                      Toast.makeText(getActivity(), "MalformedURLException", Toast.LENGTH_SHORT).show();
		                  }
		              });
		              
		              Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
		          } catch (Exception e) {
		        	  
		              dialog.dismiss();  
		              e.printStackTrace();
		              
		              getActivity().runOnUiThread(new Runnable() {
		                  public void run() {
		                	  
		                	  Toast.makeText(getActivity(), "Got Exception.", Toast.LENGTH_SHORT).show();
		                  }
		              });
		              Log.e("Upload file to server Exception", "Exception : "  + e.getMessage(), e);  
		          }
		          dialog.dismiss();       
		          return serverResponseCode; 
		          
	         } // End else block 
	       }
}
