package com.bij.auction;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bij.auction.util.JSONParser;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.facebook.android.Facebook.DialogListener;

public class LoginFragment extends Fragment {
	
	Facebook fb;
	ImageView button;
	AsyncFacebookRunner asyncRunner;
	JSONObject obj = null;
	
	ImageView login, mybid, myauction, logout;
	TextView register, welcome;
	EditText user, pass;
	String id, user_id, name, username, password, contact, email, code;
	Integer success;
			
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();
	
	SharedPreferences sp;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        
        user = (EditText)rootView.findViewById(R.id.editText1);
        pass = (EditText)rootView.findViewById(R.id.editText2);        
        login = (ImageView)rootView.findViewById(R.id.imageView1);
        register = (TextView)rootView.findViewById(R.id.textView1);
        
        button = (ImageView)rootView.findViewById(R.id.imageView2);
		welcome = (TextView)rootView.findViewById(R.id.textView2);
        
        mybid = (ImageView)rootView.findViewById(R.id.imageView10);
        myauction = (ImageView)rootView.findViewById(R.id.imageView20);
        logout = (ImageView)rootView.findViewById(R.id.imageView30);
        
        String APP_ID = getString(R.string.APP_ID);
		fb = new Facebook(APP_ID);
		asyncRunner = new AsyncFacebookRunner(fb);
		        
        sp = getActivity().getPreferences(getActivity().MODE_PRIVATE);
		String flag = sp.getString("flag", "0");
		String welcome_name = sp.getString("name", "");
        
		if(flag.equals("1"))
		{
			welcome.setText("Welcome, "+welcome_name+".");
	        login();
		}
		else
		{
			logout();
		}
		
		mybid.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MyBidFragment mb = new MyBidFragment();

				Bundle args = new Bundle();
				mb.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_login, mb);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		
		myauction.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MyAuctionFragment ma = new MyAuctionFragment();

				Bundle args = new Bundle();
				ma.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_login, ma);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});

		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fb.authorize(getActivity(), new String[]{"email"},new DialogListener() {
					
					@Override
					public void onFacebookError(FacebookError e) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "Facebook Error. Please try again.", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					public void onError(DialogError e) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "Facebook Error. Please try again.", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					public void onComplete(Bundle values) {
						// TODO Auto-generated method stub
						try{
						String jsonUser = fb.request("me");
						obj = Util.parseJson(jsonUser);
						
						new CheckFacebookLogin().execute();
						
//						Editor editor = sp.edit();
//						editor.putString("flag", "1");
//						editor.putString("user_id", id);
//						editor.putString("first_name", first_name);
//						editor.putString("last_name", last_name);
//						editor.putString("email", email);
//						editor.commit();
						
//						login();
//						
//						user.setText("");
//						pass.setText("");
						
						}catch(Exception ex){
							ex.printStackTrace();
						} catch (FacebookError e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
												
					}
					
					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
//						Toast.makeText(getActivity(), "Cancelled.", Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
		
        login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (haveNetworkConnection()) {
					if (user.getText().toString() == ""
							|| user.getText().toString() == null
							|| user.getText().length() == 0
							|| pass.getText().toString() == ""
							|| pass.getText().toString() == null
							|| pass.getText().length() == 0) {
						Toast.makeText(getActivity(),
								"Enter username and password",
								Toast.LENGTH_SHORT).show();
					} else if (user.getText().toString().contains(" ")) {
						Toast.makeText(getActivity(),
								"No spaces allowed!!", Toast.LENGTH_SHORT)
								.show();
					} else {
						new CheckLogin().execute();
					}
				} else {
					Toast.makeText(getActivity(),
							"Enable internet to login!!", Toast.LENGTH_SHORT)
							.show();
				}
				
			}
		});
        
        logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Editor editor = sp.edit();
				editor.putString("flag", "0");
				editor.putString("user_id", "");
				editor.putString("name", "");
				editor.commit();
				
				logout();
				
				try {
					fb.logout(getActivity());
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
        
        register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RegisterFragment reg = new RegisterFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
 				// Replace whatever is in the fragment_container view with this fragment,
 				// and add the transaction to the back stack
 				transaction.replace(R.id.fragment_login, reg);
 				transaction.addToBackStack(null);
 				transaction.commit();
			}
		});
         
        return rootView;
    }
	
	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}
	
	private void login()
	{
		user.setVisibility(EditText.GONE);
		pass.setVisibility(EditText.GONE);
		login.setVisibility(ImageView.GONE);
		register.setVisibility(TextView.GONE);
		button.setVisibility(ImageView.GONE);
		welcome.setVisibility(TextView.VISIBLE);
		mybid.setVisibility(ImageView.VISIBLE);
		myauction.setVisibility(ImageView.VISIBLE);
		logout.setVisibility(ImageView.VISIBLE);
	}
	private void logout()
	{
		user.setVisibility(EditText.VISIBLE);
		pass.setVisibility(EditText.VISIBLE);
		login.setVisibility(ImageView.VISIBLE);
		register.setVisibility(TextView.VISIBLE);
		button.setVisibility(ImageView.VISIBLE);
		welcome.setVisibility(TextView.GONE);
		mybid.setVisibility(ImageView.GONE);
		myauction.setVisibility(ImageView.GONE);
		logout.setVisibility(ImageView.GONE);
	}
	
	class CheckLogin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Checking..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

			getActivity().runOnUiThread(new Runnable() {
				public void run() {

					try {

						username = user.getText().toString();
						password = pass.getText().toString();

						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("username", username));
						params.add(new BasicNameValuePair("password", password));

						JSONObject json = jsonParser.makeHttpRequest(AppConstants.url_check_login,
								"POST", params);

						Log.d("Single Event Details", json.toString());

						success = json.getInt(AppConstants.TAG_SUCCESS);
						if (success == 1) {
							JSONArray empObj = json.getJSONArray(AppConstants.TAG_USERS);
							JSONObject emp = empObj.getJSONObject(0);

							user_id = emp.getString(AppConstants.TAG_USER_ID);
							name = emp.getString(AppConstants.TAG_NAME);
//							username = emp.getString(AppConstants.TAG_USERNAME);
//							contact = emp.getString(AppConstants.TAG_CONTACT);
//							email = emp.getString(AppConstants.TAG_EMAIL);

						} 
						else if(success == 2)
						{
							JSONArray empObj = json.getJSONArray(AppConstants.TAG_USERS);
							JSONObject emp = empObj.getJSONObject(0);

							user_id = emp.getString(AppConstants.TAG_USER_ID);
							name = emp.getString(AppConstants.TAG_NAME);
							code = emp.getString(AppConstants.TAG_CODE);
							
							Log.d("CODE", code);
						}
						else if(success == 0){
							Toast.makeText(getActivity(),
									"Username/Password mistaken!!",
									Toast.LENGTH_SHORT).show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			if(success == 1)
			{
				Editor editor = sp.edit();
				editor.putString("flag", "1");
				editor.putString("user_id", user_id);
				editor.putString("name", name);
				editor.commit();

				welcome.setText("Welcome, "+name+".");
				login();
				
				user.setText("");
				pass.setText("");
			}
			else if(success == 2)
			{
//				Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
				/*VerificationFragment vf = new VerificationFragment();

				Bundle args = new Bundle();
				args.putString("code", code);
				vf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_login, vf);
				transaction.addToBackStack(null);
				transaction.commit();*/
				
				Editor editor = sp.edit();
				editor.putString("flag", "1");
				editor.putString("user_id", user_id);
				editor.putString("name", name);
				editor.commit();

				welcome.setText("Welcome, "+name+".");
				login();
				
				user.setText("");
				pass.setText("");

			}
		}

	}
	
	class CheckFacebookLogin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Checking..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

			getActivity().runOnUiThread(new Runnable() {
				public void run() {

					try {
						id = obj.optString("id");
						String full_name = obj.optString("name");
						String first_name = obj.optString("first_name");
						String last_name = obj.optString("last_name");
						String gender = obj.optString("gender");
						String email = obj.optString("email");
						
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("fb_id", id));
						params.add(new BasicNameValuePair("first_name", first_name));
						params.add(new BasicNameValuePair("last_name", last_name));
						params.add(new BasicNameValuePair("gender", gender));
						params.add(new BasicNameValuePair("email", email));

						JSONObject json = jsonParser.makeHttpRequest(AppConstants.url_check_login,
								"POST", params);

						Log.d("FACEBOOK INFO", json.toString());

						success = json.getInt(AppConstants.TAG_SUCCESS);
						if(success == 1)
						{
							JSONArray empObj = json.getJSONArray(AppConstants.TAG_USERS);
							JSONObject emp = empObj.getJSONObject(0);

							user_id = emp.getString(AppConstants.TAG_USER_ID);
							name = emp.getString(AppConstants.TAG_NAME);
						}
						else if(success == 2)
						{
							JSONArray empObj = json.getJSONArray(AppConstants.TAG_USERS);
							JSONObject emp = empObj.getJSONObject(0);

							user_id = emp.getString(AppConstants.TAG_USER_ID);
							name = emp.getString(AppConstants.TAG_NAME);
							code = emp.getString(AppConstants.TAG_CODE);
							
							Log.d("CODE", code);
						}
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			if(success == 1)
			{				
//				Toast.makeText(getActivity(), "success=1", Toast.LENGTH_LONG).show();
				Editor editor = sp.edit();
				editor.putString("flag", "1");
				editor.putString("user_id", user_id);
				editor.putString("name", name);
				editor.commit();
				
				welcome.setText("Welcome, "+name+".");
				login();
				
				user.setText("");
				pass.setText("");
			}
			else if(success == 2)
			{
//				Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
				VerificationFragment vf = new VerificationFragment();

				Bundle args = new Bundle();
				args.putString("code", code);
				vf.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_login, vf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		}

	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		fb.authorizeCallback(requestCode, resultCode, data);
	}
}
