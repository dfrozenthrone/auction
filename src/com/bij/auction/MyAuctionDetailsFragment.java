package com.bij.auction;

import com.bij.auction.util.ImageLoader;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAuctionDetailsFragment extends Fragment{
	
	String auction_type, title, category, description, picture_link, amount_increment, bid_price, expiry_time, buy_price, status;
	ImageView image, btn;
	TextView tTitle, tPrice, tExpiry, tDescription, tBuyPrice, tStatus;
	ImageLoader imageLoader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_item_details, container, false);
		
		auction_type = getArguments().getString(AppConstants.TAG_AUCTION_TYPE);
		category = getArguments().getString(AppConstants.TAG_CATEGORY_NAME);
		title = getArguments().getString(AppConstants.TAG_TITLE);
		description = getArguments().getString(AppConstants.TAG_DESCRIPTION);
		picture_link = getArguments().getString(AppConstants.TAG_PICTURE_LINK);
		amount_increment = getArguments().getString(AppConstants.TAG_AMOUNT_INCREMENT);
		bid_price = getArguments().getString(AppConstants.TAG_BID_PRICE);
		expiry_time = getArguments().getString(AppConstants.TAG_EXPIRY_TIME);
		buy_price = getArguments().getString(AppConstants.TAG_EXPECTED_PRICE);
		status = getArguments().getString(AppConstants.TAG_STATUS);
		
		image = (ImageView)rootView.findViewById(R.id.imageView1);
		btn = (ImageView)rootView.findViewById(R.id.imageView2);
		tTitle = (TextView)rootView.findViewById(R.id.textView1);
		tPrice = (TextView)rootView.findViewById(R.id.textView5);
		tBuyPrice = (TextView)rootView.findViewById(R.id.textView6);
		tExpiry = (TextView)rootView.findViewById(R.id.textView3);
		tDescription = (TextView)rootView.findViewById(R.id.textView4);
		tStatus = (TextView)rootView.findViewById(R.id.textView7);
		
		
		tTitle.setText(title);
		tPrice.setText(Html.fromHtml("Rs. " +bid_price));
		tExpiry.setText(Html.fromHtml("Expires: " +expiry_time));
		tDescription.setText(Html.fromHtml("<b>Description: </b><br/>" +description));
		if(status.equals("3"))
		{
			tStatus.setVisibility(TextView.VISIBLE);
			tStatus.setText(Html.fromHtml("<b>Status: </b>Sold out"));
		}
		else if(status.equals("2"))
		{
			tStatus.setVisibility(TextView.VISIBLE);
			tStatus.setText(Html.fromHtml("<b>Status: </b>Pending"));
		}
		tBuyPrice.setText(Html.fromHtml("Buy Price Rs. " +buy_price));
		
		imageLoader=new ImageLoader(getActivity());
		imageLoader.DisplayImage(picture_link , image);
		
		return rootView;
	}

}
