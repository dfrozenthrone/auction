package com.bij.auction;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ItemDescriptionFragment extends Fragment{
	
	ImageView iv;
	EditText et1, et2, et4, et3;
	RadioButton rb1, rb2;
	String auction_type;
	
	SharedPreferences sp;
    Editor edt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_item_description, container, false);
		
		sp = getActivity().getSharedPreferences("newFile", Context.MODE_PRIVATE);
		
		et1 = (EditText)rootView.findViewById(R.id.editText1);
		et2 = (EditText)rootView.findViewById(R.id.editText2);
		et3 = (EditText)rootView.findViewById(R.id.editText3);
		et4 = (EditText)rootView.findViewById(R.id.editText4);
		rb1 = (RadioButton)rootView.findViewById(R.id.radio1);
		rb2 = (RadioButton)rootView.findViewById(R.id.radio2);
		
		et1.setText(sp.getString("title", ""));
		et2.setText(sp.getString("description", ""));
		et3.setText(sp.getString("amount_increment", ""));
		et4.setText(sp.getString("expected_price", ""));
		
		auction_type = sp.getString("auction_type", "bid");
		if(auction_type.equals("buy"))
		{
			rb1.setChecked(false);
			rb2.setChecked(true);
			auction_type = "buy";
		}
		else
		{
			rb1.setChecked(true);
			rb2.setChecked(false);
			et3.setVisibility(EditText.VISIBLE);
			auction_type = "bid";
		}
			
		rb1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				auction_type = "bid";
				et3.setVisibility(EditText.VISIBLE);
			}
		});
		
		rb2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				auction_type = "buy";
				et3.setVisibility(EditText.GONE);
			}
		});
		
		iv = (ImageView)rootView.findViewById(R.id.imageView1);
		iv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(auction_type.equals("bid") && et3.getText().toString().trim().equals(""))
				{
					Toast.makeText(getActivity(), "Fill all fields.", Toast.LENGTH_SHORT).show();
				}
				else if(et1.getText().toString().trim().equals("") ||
					et2.getText().toString().trim().equals("") ||
					et4.getText().toString().trim().equals("") ||
					et4.getText().toString().trim().equals("."))
				{
					Toast.makeText(getActivity(), "Fill all fields.", Toast.LENGTH_SHORT).show();
				}
				else
				{
					edt = sp.edit();
					edt.putString("title", et1.getText().toString().trim());
					edt.putString("description", et2.getText().toString().trim());
					edt.putString("expected_price", et4.getText().toString().trim());
					edt.putString("auction_type", auction_type);
					edt.putString("amount_increment", et3.getText().toString().trim());
					edt.commit();
					Toast.makeText(getActivity(), "Saved.", Toast.LENGTH_SHORT).show();
					getActivity().getFragmentManager().popBackStack();
				}
//				Toast.makeText(getActivity(), auction_type, Toast.LENGTH_SHORT).show();
			}
		});
		
		return rootView;
	}

}
