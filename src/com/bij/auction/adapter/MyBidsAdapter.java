package com.bij.auction.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bij.auction.AppConstants;
import com.bij.auction.R;
import com.bij.auction.adapter.MyAuctionsAdapter.ViewHolder;
import com.bij.auction.util.ImageLoader;

public class MyBidsAdapter extends ArrayAdapter<HashMap<String, String>> {
	private Activity activity;
	ArrayList<HashMap<String, String>> my_bidList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;

	public MyBidsAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
//		Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		my_bidList = objects;
		imageLoader=new ImageLoader(context);
//		Log.d("WHERE M I1???????", "U R HERE!!!!!!!");
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageView1);
			holder.tTitle = (TextView) v.findViewById(R.id.textView1);
			holder.tStatus = (TextView) v.findViewById(R.id.textView2);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		Log.d("ITEM NAME:::::::::", my_bidList.get(position).get("auction_id"));
		
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(my_bidList.get(position).get(AppConstants.TAG_PICTURE_LINK) , holder.imageview);
		holder.tTitle.setText(my_bidList.get(position).get(AppConstants.TAG_TITLE).toString());
		if(my_bidList.get(position).get(AppConstants.TAG_STATUS).toString().equals("Winning"))
		{
			holder.tStatus.setTextColor(Color.parseColor("#0099cc"));
		}
		else
		{
			holder.tStatus.setTextColor(Color.parseColor("#ff0000"));
		}
		holder.tStatus.setText(my_bidList.get(position).get(AppConstants.TAG_STATUS).toString());
		
		return v;

	}

	static class ViewHolder {
		public ImageView imageview;
		public TextView tTitle;
		public TextView tStatus;
	}

//	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//		ImageView bmImage;
//
//		public DownloadImageTask(ImageView bmImage) {
//			this.bmImage = bmImage;
//		}
//
//		protected Bitmap doInBackground(String... urls) {
//			String urldisplay = urls[0];
//			Bitmap mIcon11 = null;
//			try {
//				InputStream in = new java.net.URL(urldisplay).openStream();
//				mIcon11 = BitmapFactory.decodeStream(in);
//			} catch (Exception e) {
//				Log.e("Error", e.getMessage());
//				e.printStackTrace();
//			}
//			return mIcon11;
//		}
//
//		protected void onPostExecute(Bitmap result) {
//			bmImage.setImageBitmap(result);
//		}
//
//	}
}
