package com.bij.auction.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bij.auction.AppConstants;
import com.bij.auction.R;
import com.bij.auction.adapter.AuctionsAdapter.ViewHolder;
import com.bij.auction.util.ImageLoader;

public class MyAuctionsAdapter extends ArrayAdapter<HashMap<String, String>> {
	private Activity activity;
	ArrayList<HashMap<String, String>> my_auctionList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;

	public MyAuctionsAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
//		Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		my_auctionList = objects;
		imageLoader=new ImageLoader(context);
//		Log.d("WHERE M I1???????", "U R HERE!!!!!!!");
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageView1);
			holder.tTitle = (TextView) v.findViewById(R.id.textView1);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

//		Log.d("ITEM NAME:::::::::", my_auctionList.get(position).get("auction_id"));
		
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(my_auctionList.get(position).get(AppConstants.TAG_PICTURE_LINK) , holder.imageview);
//		new DownloadImageTask(holder.imageview).execute(auctionList.get(position).get("picture_link"));
		holder.tTitle.setText(my_auctionList.get(position).get(AppConstants.TAG_TITLE).toString());
		
		return v;

	}

	static class ViewHolder {
		public ImageView imageview;
		public TextView tId;
		public TextView tId1;
		public TextView tId2;
		public TextView tTitle;
		public TextView temp;
		public TextView tPrice;
		public TextView tExpiry;
	}

//	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//		ImageView bmImage;
//
//		public DownloadImageTask(ImageView bmImage) {
//			this.bmImage = bmImage;
//		}
//
//		protected Bitmap doInBackground(String... urls) {
//			String urldisplay = urls[0];
//			Bitmap mIcon11 = null;
//			try {
//				InputStream in = new java.net.URL(urldisplay).openStream();
//				mIcon11 = BitmapFactory.decodeStream(in);
//			} catch (Exception e) {
//				Log.e("Error", e.getMessage());
//				e.printStackTrace();
//			}
//			return mIcon11;
//		}
//
//		protected void onPostExecute(Bitmap result) {
//			bmImage.setImageBitmap(result);
//		}
//
//	}
}
