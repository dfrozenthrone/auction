package com.bij.auction.adapter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.bij.auction.AppConstants;
import com.bij.auction.R;
import com.bij.auction.util.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AuctionsAdapter extends ArrayAdapter<HashMap<String, String>> {
	private Activity activity;
	ArrayList<HashMap<String, String>> auctionList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;
//	public ImageView imageview;
//	public TextView tName, tDate;

	public AuctionsAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
//		Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		auctionList = objects;
		imageLoader=new ImageLoader(context);
//		Log.d("WHERE M I1???????", "U R HERE!!!!!!!");
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageView1);
			holder.tId = (TextView) v.findViewById(R.id.auction_id);
			holder.tId1 = (TextView) v.findViewById(R.id.category_id);
			holder.tId2 = (TextView) v.findViewById(R.id.owner_id);
			holder.tTitle = (TextView) v.findViewById(R.id.textView1);
			holder.temp = (TextView) v.findViewById(R.id.textView2);
			holder.tPrice = (TextView) v.findViewById(R.id.textView3);
			holder.tExpiry = (TextView) v.findViewById(R.id.textView4);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		Log.d("ITEM NAME:::::::::", auctionList.get(position).get("auction_id"));
		
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(auctionList.get(position).get(AppConstants.TAG_PICTURE_LINK) , holder.imageview);
//		new DownloadImageTask(holder.imageview).execute(auctionList.get(position).get("picture_link"));
		holder.tId.setText(auctionList.get(position).get(AppConstants.TAG_AUCTION_ID).toString());
		holder.tId1.setText(auctionList.get(position).get(AppConstants.TAG_CATEGORY_ID).toString());
		holder.tId2.setText(auctionList.get(position).get(AppConstants.TAG_OWNER_ID).toString());
		holder.tTitle.setText(auctionList.get(position).get(AppConstants.TAG_TITLE).toString());
//		holder.tPrice.setText("Rs. " + auctionList.get(position).get(AppConstants.TAG_BID_PRICE).toString());
		holder.tExpiry.setText("Expires at " + auctionList.get(position).get(AppConstants.TAG_EXPIRY_TIME).toString());
		if(auctionList.get(position).get(AppConstants.TAG_AUCTION_TYPE).toString().equals("buy"))
		{
//			holder.temp.setVisibility(TextView.INVISIBLE);
//			holder.tPrice.setVisibility(TextView.INVISIBLE);
			holder.temp.setText("Buy Price ");
			holder.tPrice.setText("Rs. " + auctionList.get(position).get(AppConstants.TAG_EXPECTED_PRICE).toString());
		}
		else
		{
			holder.temp.setText("Current Bid ");
			holder.tPrice.setText("Rs. " + auctionList.get(position).get(AppConstants.TAG_BID_PRICE).toString());
		}
		
		return v;

	}

	static class ViewHolder {
		public ImageView imageview;
		public TextView tId;
		public TextView tId1;
		public TextView tId2;
		public TextView tTitle;
		public TextView temp;
		public TextView tPrice;
		public TextView tExpiry;
	}

//	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//		ImageView bmImage;
//
//		public DownloadImageTask(ImageView bmImage) {
//			this.bmImage = bmImage;
//		}
//
//		protected Bitmap doInBackground(String... urls) {
//			String urldisplay = urls[0];
//			Bitmap mIcon11 = null;
//			try {
//				InputStream in = new java.net.URL(urldisplay).openStream();
//				mIcon11 = BitmapFactory.decodeStream(in);
//			} catch (Exception e) {
//				Log.e("Error", e.getMessage());
//				e.printStackTrace();
//			}
//			return mIcon11;
//		}
//
//		protected void onPostExecute(Bitmap result) {
//			bmImage.setImageBitmap(result);
//		}
//
//	}
}
