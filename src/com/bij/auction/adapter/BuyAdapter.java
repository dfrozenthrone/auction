package com.bij.auction.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bij.auction.R;
import com.bij.auction.util.ImageLoader;

public class BuyAdapter extends ArrayAdapter<HashMap<String, String>>{

	private Activity activity;
	ArrayList<HashMap<String, String>> categoryList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;
	
	public BuyAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		categoryList = objects;
		imageLoader=new ImageLoader(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.grid_item_image);
			holder.tId = (TextView) v.findViewById(R.id.grid_category_id);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(categoryList.get(position).get("category_image") , holder.imageview);
//		new DownloadImageTask(holder.imageview).execute(eventList.get(position).get("picture_link"));
		holder.tId.setText(categoryList.get(position).get("category_id").toString());
		Log.d("ITEM NAME:::::::::", categoryList.get(position).get("category_id"));
		return v;
	}
	
	static class ViewHolder {
		public ImageView imageview;
		public TextView tId;

	}
	
}
