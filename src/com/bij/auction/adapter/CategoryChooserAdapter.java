package com.bij.auction.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bij.auction.R;
import com.bij.auction.adapter.BuyAdapter.ViewHolder;
import com.bij.auction.util.ImageLoader;

public class CategoryChooserAdapter extends ArrayAdapter<HashMap<String, String>>{
	
	private Activity activity;
	ArrayList<HashMap<String, String>> categoryList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;
	
	public CategoryChooserAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		categoryList = objects;
		imageLoader=new ImageLoader(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageView1);
			holder.tCategory = (TextView) v.findViewById(R.id.textView1);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(categoryList.get(position).get("category_image") , holder.imageview);
//		new DownloadImageTask(holder.imageview).execute(eventList.get(position).get("picture_link"));
		holder.tCategory.setText(categoryList.get(position).get("category_name").toString());
//		Log.d("ITEM NAME:::::::::", categoryList.get(position).get("category_id"));
		return v;
	}
	
	static class ViewHolder {
		public ImageView imageview;
		public TextView tCategory;

	}

}
