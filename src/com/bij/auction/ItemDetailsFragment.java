package com.bij.auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bij.auction.database.DatabaseHandler;
import com.bij.auction.model.AuctionsModel;
import com.bij.auction.util.ImageLoader;
import com.bij.auction.util.JSONParser;

public class ItemDetailsFragment extends Fragment{
	
	String auction_id, auction_type, category_id, owner_id, category, title, description, picture_link, start_price, amount_increment, bid_price, expected_price, owner_name, email, contact_no, expiry_time;
	ImageView image, btn, buy;
	TextView tTitle, tPrice, tExpiry, tDescription, temp;
	ImageLoader imageLoader;
	
	ArrayList<HashMap<String, String>> auction;
	DatabaseHandler db;
	
	JSONParser jParser = new JSONParser();
	JSONArray auctions = null;
//	ArrayList<HashMap<String, String>> auctionsList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_item_details, container, false);
		
		auction_id = getArguments().getString("auction_id");
		auction_type = getArguments().getString("auction_type");
		
		image = (ImageView)rootView.findViewById(R.id.imageView1);
		btn = (ImageView)rootView.findViewById(R.id.imageView2);
		buy = (ImageView)rootView.findViewById(R.id.imageView3);
		tTitle = (TextView)rootView.findViewById(R.id.textView1);
		temp = (TextView)rootView.findViewById(R.id.textView2);
		tPrice = (TextView)rootView.findViewById(R.id.textView5);
		tExpiry = (TextView)rootView.findViewById(R.id.textView3);
		tDescription = (TextView)rootView.findViewById(R.id.textView4);
		
		auction = new ArrayList<HashMap<String,String>>();
		db = new DatabaseHandler(getActivity());
		
		if(auction_type.equals("bid"))
		{
			btn.setVisibility(ImageView.VISIBLE);
			buy.setVisibility(ImageView.GONE);
		}
		else if(auction_type.equals("buy"))
		{
			btn.setVisibility(ImageView.GONE);
			buy.setVisibility(ImageView.VISIBLE);
		}
		
		List<AuctionsModel> list = db.getAuction(auction_id);
		for(AuctionsModel i : list)
		{
			category_id = i.getCategory_id();
			owner_id = i.getOwner_id();
			category = i.getCategory();
			title = i.getTitle();
			description = i.getDescription();
			picture_link = i.getPicture_link();
			start_price = i.getStart_price();
			amount_increment = i.getAmount_increment();
			bid_price = i.getBid_price();
			expected_price = i.getExpected_price();
			owner_name = i.getOwner_name();
			email = i.getEmail();
			contact_no = i.getContact_no();
			expiry_time = i.getExpiry_time();
		}
		
		tTitle.setText(title);
		
		if(auction_type.equals("buy")){
			tPrice.setVisibility(View.GONE);
			temp.setVisibility(View.GONE);
		} else if (!bid_price.equals(null)) {
			tPrice.setText(Html.fromHtml("Rs. " +bid_price));
		} else {
			tPrice.setVisibility(View.GONE);
		}
		
		if (!expiry_time.equals(null)) {
			tExpiry.setText(Html.fromHtml("Expires: " +expiry_time));
		} else {
			tExpiry.setVisibility(View.GONE);
		}
		
		if (!description.equals(null)) {
			tDescription.setText(Html.fromHtml("<b>Description: </b><br/>" +description));
		} else {
			tDescription.setVisibility(View.GONE);
		}
		
		imageLoader=new ImageLoader(getActivity());
		imageLoader.DisplayImage(picture_link , image);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PlaceBidFragment pb = new PlaceBidFragment();

				Bundle args = new Bundle();
				args.putString("bid_or_buy", "bid");
				args.putString("auction_id", auction_id);
				args.putString("bid_price", bid_price);
				args.putString("amount_increment", amount_increment);
				args.putString("expected_price", expected_price);
				pb.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_item_details, pb);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});		
		
		buy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PlaceBidFragment pb = new PlaceBidFragment();

				Bundle args = new Bundle();
				args.putString("bid_or_buy", "buy");
				args.putString("auction_id", auction_id);
				args.putString("bid_price", bid_price);
				args.putString("amount_increment", amount_increment);
				args.putString("expected_price", expected_price);
				pb.setArguments(args);
				
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_item_details, pb);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		
		return rootView;
	}

}
